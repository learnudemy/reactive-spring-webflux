package com.learnreactiveprogramming.service;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple4;

import java.time.Duration;
import java.util.List;
import java.util.Random;
import java.util.function.UnaryOperator;

public class FluxAndMonoGeneratorService {

    public Flux<String> namesFlux() {
        return Flux.fromIterable(List.of("Guso", "Seba", "Oliv"))
                .log(); //db or a remote service call
    }

    public Mono<String> nameMono() {
        return Mono.just("Gus");
    }

    public Flux<String> namesFluxApplyMap(int filterLength) {
        return Flux.fromIterable(List.of("guso", "seb", "oliv"))
                .map(String::toUpperCase)
                .filter(n -> n.length() > filterLength)
                .map(n -> n.length() + " - " + n)
                .log(); //db or a remote service call
    }

    public Flux<String> namesFluxImmutability() {
        var nameFlux = Flux.fromIterable(List.of("guso", "seba", "oliv"));
        nameFlux.map(String::toUpperCase);
        return nameFlux;
    }

    public Flux<String> namesFluxApplyFlatMap(int filterLength) {
        return Flux.fromIterable(List.of("guso", "seb", "oliv"))
                .map(String::toUpperCase)
                .filter(n -> n.length() > filterLength)
                .flatMap(n -> splitString(n))
                .log(); //db or a remote service call
    }

    public Mono<String> namesMonoMapFilter(int filterLength) {
        return Mono.just("alex")
                .map(String::toUpperCase)
                .filter(n-> n.length() > filterLength);
    }

    public Mono<List<String>> namesMonoFlatMap(int filterLength) {
        return Mono.just("guso")
                .map(String::toUpperCase)
                .filter(n -> n.length() > filterLength)
                .flatMap(this::splitStringMono)
                .log();
    }

    public Flux<String> namesMonoFlatMapMany(int filterLength) {
        return Mono.just("guso")
                .map(String::toUpperCase)
                .filter(n -> n.length() > filterLength)
                .flatMapMany(this::splitString)
                .log();
    }

    public Flux<String> namesFluxApplyFlatMapAsync(int filterLength) {
        return Flux.fromIterable(List.of("guso", "seb", "oliv"))
                .map(String::toUpperCase)
                .filter(n -> n.length() > filterLength)
                .flatMap(n -> splitStringWithDelay(n))
                .log(); //db or a remote service call
    }

    public Flux<String> namesFluxApplyConcatMap(int filterLength) {
        return Flux.fromIterable(List.of("guso", "seb", "oliv"))
                .map(String::toUpperCase)
                .filter(n -> n.length() > filterLength)
                .concatMap(n -> splitStringWithDelay(n))
                .log(); //db or a remote service call
    }

    public Flux<String> namesFluxApplyTransform(int filterLength) {

        UnaryOperator<Flux<String>> filterMap = name -> name.map(String::toUpperCase)
                .filter(n -> n.length() > filterLength);

        return Flux.fromIterable(List.of("guso", "seb", "oliv"))
                .transform(filterMap)
                .flatMap(this::splitString)
                .defaultIfEmpty("default")
                .log(); //db or a remote service call
    }

    public Flux<String> namesFluxApplyTransformSwitchIfEmpty(int filterLength) {

        UnaryOperator<Flux<String>> filterMap = name ->
                name.map(String::toUpperCase)
                        .filter(n -> n.length() > filterLength)
                        .flatMap(this::splitString);

        Flux<String> defaultFlux = Flux.just("default")
                .transform(filterMap);

        return Flux.fromIterable(List.of("guso", "seb", "oliv"))
                .transform(filterMap)
                .switchIfEmpty(defaultFlux)
                .log(); //db or a remote service call
    }

    public Flux<String> exploreConcat(){
        var abcFlux = Flux.just("A", "B", "C");
        var defFlux = Flux.just("D", "E", "F");

        return Flux.concat(abcFlux, defFlux)
                .log();
    }

    public Flux<String> exploreConcatWhit(){
        var abcFlux = Flux.just("A", "B", "C");
        var defFlux = Flux.just("D", "E", "F");

        return abcFlux.concatWith(defFlux)
                .log();
    }

    public Flux<String> exploreConcatWhitInMono(){
        var aMono = Mono.just("A");
        var bMono = Mono.just("B");

        return aMono.concatWith(bMono)
                .log();
    }

    public Flux<String> exploreMerge(){
        var abcFlux = Flux.just("A", "B", "C")
                .delayElements(Duration.ofMillis(100));
        var defFlux = Flux.just("D", "E", "F")
                .delayElements(Duration.ofMillis(125));

        return Flux.merge(abcFlux, defFlux)
                .log();
    }

    public Flux<String> exploreMergeWith(){
        var abcFlux = Flux.just("A", "B", "C")
                .delayElements(Duration.ofMillis(100));
        var defFlux = Flux.just("D", "E", "F")
                .delayElements(Duration.ofMillis(125));

        return abcFlux.mergeWith(defFlux)
                .log();
    }

    public Flux<String> exploreMergeWithMono(){
        var aMono = Mono.just("A")
                .delayElement(Duration.ofMillis(100));
        var bMono = Mono.just("B")
                .delayElement(Duration.ofMillis(125));

        return aMono.mergeWith(bMono)
                .log();
    }

    public Flux<String> exploreMergeSequential(){
        var abcFlux = Flux.just("A", "B", "C")
                .delayElements(Duration.ofMillis(100));
        var defFlux = Flux.just("D", "E", "F")
                .delayElements(Duration.ofMillis(125));

        return Flux.mergeSequential(abcFlux, defFlux)
                .log();
    }

    public Flux<String> exploreZip(){
        var abcFlux = Flux.just("A", "B", "C");

        var defFlux = Flux.just("D", "E", "F");

        return Flux.zip(abcFlux, defFlux, ((first, second) -> first + second))
                .log();
    }

    public Flux<String> exploreZipWith(){
        var abcFlux = Flux.just("A", "B", "C");

        var defFlux = Flux.just("D", "E", "F");

        return abcFlux.zipWith(defFlux, ((first, second) -> first + second))
                .log();
    }

    public Flux<String> exploreZipTuple(){
        var abcFlux = Flux.just("A", "B", "C");

        var defFlux = Flux.just("D", "E", "F");

        var _123 = Flux.just("1", "2", "3");

        var _456 = Flux.just("4", "5", "6");

        return Flux.zip(abcFlux, defFlux, _123, _456)
                .map(t4 -> t4.getT1() + t4.getT2() + t4.getT3() + t4.getT4() )
                .log();
    }

    public Mono<String> exploreZipWithMono(){
        var aMono = Mono.just("A");
        var bMono = Mono.just("B");

        return aMono.zipWith(bMono)
                .map(t2 -> t2.getT1() + t2.getT2())
                .log();
    }

    private Flux<String> splitString(String name) {
        String[] charArray = name.split("");
        return Flux.fromArray(charArray);
    }

    private Flux<String> splitStringWithDelay(String name) {
        String[] charArray = name.split("");
        var delay = new Random().nextInt(1000);
        return Flux.fromArray(charArray)
                .delayElements(Duration.ofMillis(delay));
    }

    private Mono<List<String>> splitStringMono(String name) {
        String[] charArray = name.split("");
        var charList = List.of(charArray);
        return Mono.just(charList);
    }

    public static void main(String[] args) {
        FluxAndMonoGeneratorService service = new FluxAndMonoGeneratorService();

        service.namesFlux()
                .subscribe(name -> {
                    System.out.println("Name is : " + name);
                });

        service.nameMono()
                .subscribe(name -> {
                    System.out.println("Mono name is : " + name);
                });
    }
}
