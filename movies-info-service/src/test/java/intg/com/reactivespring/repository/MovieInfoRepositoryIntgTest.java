package com.reactivespring.repository;

import com.mongodb.reactivestreams.client.MongoClient;
import com.reactivespring.domain.MovieInfo;
import de.flapdoodle.embed.mongo.MongodExecutable;
import de.flapdoodle.embed.mongo.MongodProcess;
import de.flapdoodle.embed.mongo.MongodStarter;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.test.context.ActiveProfiles;
import reactor.test.StepVerifier;

import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DataMongoTest
@ActiveProfiles("test")
class MovieInfoRepositoryIntgTest {

    @Autowired
    MovieInfoRepository movieInfoRepository;

    @BeforeEach
    void setUp(){
        var movieInfos = List.of(new MovieInfo(null, "Batman Begins",
                        2005, List.of("Christian Bale", "Michael Cane"), LocalDate.parse("2005-06-15")),
                new MovieInfo(null, "The Dark Knight",
                        2008, List.of("Christian Bale", "HeathLedger"), LocalDate.parse("2008-07-18")),
                new MovieInfo("abc", "Dark Knight Rises",
                        2012, List.of("Christian Bale", "Tom Hardy"), LocalDate.parse("2012-07-20")));

        movieInfoRepository.saveAll(movieInfos)
                .blockLast();
    }

    @AfterEach
    void tearDown(){
        movieInfoRepository.deleteAll().block();
    }

    @Test
    void findAll(){
        //Arrange

        //Act
        var moviesInfoFlux = movieInfoRepository.findAll();

        //Assert
        StepVerifier.create(moviesInfoFlux)
                .expectNextCount(3)
                .verifyComplete();
    }

    @Test
    void findById(){
        //Arrange

        //Act
        var moviesInfoFlux = movieInfoRepository.findById("abc");

        //Assert
        StepVerifier.create(moviesInfoFlux)
                //.expectNextCount(1)
                .assertNext(movieInfo -> {
                    assertEquals("Dark Knight Rises", movieInfo.getName());
                })
                .verifyComplete();
    }

    @Test
    void saveMovieInfo(){
        //Arrange
        var movieInfo = new MovieInfo(null, "Batman Begins 1",
                2005, List.of("Christian Bale", "Michael Cane"), LocalDate.parse("2005-06-15"));

        //Act
        var moviesInfoFlux = movieInfoRepository.save(movieInfo).log();

        //Assert
        StepVerifier.create(moviesInfoFlux)
                //.expectNextCount(1)
                .assertNext(movieInfo1 -> {
                    assertEquals("Batman Begins 1", movieInfo.getName());
                })
                .verifyComplete();
    }

    @Test
    void updateMovieInfo(){
        //Arrange
        var movieInfo = movieInfoRepository.findById("abc").block();
        movieInfo.setYear(2021);

        //Act
        var moviesInfoFlux = movieInfoRepository.save(movieInfo).log();

        //Assert
        StepVerifier.create(moviesInfoFlux)
                //.expectNextCount(1)
                .assertNext(movieInfo1 -> {
                    assertEquals(2021, movieInfo.getYear());
                })
                .verifyComplete();
    }

    @Test
    void deleteMovieInfo(){
        //Arrange
        movieInfoRepository.deleteById("abc").block();

        //Act
        var moviesInfoFlux = movieInfoRepository.findAll();

        //Assert
        StepVerifier.create(moviesInfoFlux)
                .expectNextCount(2)
                .verifyComplete();
    }

    @Test
    void findYear(){
        //Arrange

        //Act
        var moviesInfoFlux = movieInfoRepository.findByYear(2005);

        //Assert
        StepVerifier.create(moviesInfoFlux)
                .expectNextCount(1)
                .verifyComplete();
    }

    @Test
    void findByName() {

        var movieInfosMono = movieInfoRepository.findByName("Batman Begins").log();

        StepVerifier.create(movieInfosMono)
                .expectNextCount(1)
                .verifyComplete();


    }

}