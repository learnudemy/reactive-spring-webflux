package com.learnreactiveprogramming.service;

import org.junit.jupiter.api.Test;
import reactor.test.StepVerifier;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class FluxAndMonoGeneratorServiceTest {
    FluxAndMonoGeneratorService fluxAndMonoGeneratorService = new FluxAndMonoGeneratorService();

    @Test
    void namesFlux() {
        //Arrange

        //Act
        var nameFlux = fluxAndMonoGeneratorService.namesFlux();

        //Assert
        StepVerifier.create(nameFlux)
                //.expectNext("Guso", "Seba", "Oliv")
                //.expectNext("Seba", "Guso", "Oliv")
                //.expectNextCount(3)
                .expectNext("Guso")
                .expectNextCount(2)
                .verifyComplete();
    }

    @Test
    void namesFluxApplyMap() {
        //Arrange
        int filterLength = 3;

        //Act
        var names = fluxAndMonoGeneratorService.namesFluxApplyMap(filterLength);

        //Assert
        StepVerifier.create(names)
                .expectNext("4 - GUSO", "4 - OLIV")
                //.expectNextCount(2)
                .verifyComplete();
    }

    @Test
    void namesFluxImmutability() {
        //Arrange

        //Act
        var names = fluxAndMonoGeneratorService.namesFluxImmutability();

        //Assert
        StepVerifier.create(names)
                .expectNext("guso", "seba", "oliv")
                .verifyComplete();
    }

    @Test
    void namesFluxApplyFlatMap() {
        //Arrange
        int filterLength = 3;

        //Act
        var namesFlux = fluxAndMonoGeneratorService.namesFluxApplyFlatMap(filterLength);

        //Assert
        StepVerifier.create(namesFlux)
                .expectNext("G", "U", "S", "O", "O", "L", "I", "V")
                .verifyComplete();
    }

    @Test
    void namesFluxApplyFlatMapAsync() {
        //Arrange
        int filterLength = 3;

        //Act
        var namesFlux = fluxAndMonoGeneratorService.namesFluxApplyFlatMapAsync(filterLength);

        //Assert
        StepVerifier.create(namesFlux)
                .expectNextCount(8)
                .verifyComplete();
    }

    @Test
    void namesFluxApplyConcatMap() {
        //Arrange
        int filterLength = 3;

        //Act
        var namesFlux = fluxAndMonoGeneratorService.namesFluxApplyConcatMap(filterLength);

        //Assert
        StepVerifier.create(namesFlux)
                .expectNext("G", "U", "S", "O", "O", "L", "I", "V")
                .verifyComplete();
    }

    @Test
    void namesMonoFlatMap() {
        //Arrange
        int filterLength = 3;

        //Act
        var value = fluxAndMonoGeneratorService.namesMonoFlatMap(filterLength);

        //Assert
        StepVerifier.create(value)
                .expectNext(List.of("G", "U", "S", "O"))
                .verifyComplete();
    }

    @Test
    void namesMonoFlatMapMany() {
        //Arrange
        int filterLength = 3;

        //Act
        var value = fluxAndMonoGeneratorService.namesMonoFlatMapMany(filterLength);

        //Assert
        StepVerifier.create(value)
                .expectNext("G", "U", "S", "O")
                .verifyComplete();
    }

    @Test
    void namesFluxApplyTransform() {
        //Arrange
        int filterLength = 3;

        //Act
        var namesFlux = fluxAndMonoGeneratorService.namesFluxApplyTransform(filterLength);

        //Assert
        StepVerifier.create(namesFlux)
                .expectNext("G", "U", "S", "O", "O", "L", "I", "V")
                .verifyComplete();
    }

    @Test
    void namesFluxApplyTransformDefaultIfEmpty() {
        //Arrange
        int filterLength = 6;

        //Act
        var namesFlux = fluxAndMonoGeneratorService.namesFluxApplyTransform(filterLength);

        //Assert
        StepVerifier.create(namesFlux)
                .expectNext("default")
                .verifyComplete();
    }

    @Test
    void namesFluxApplyTransformSwitchIfEmpty() {
        //Arrange
        int filterLength = 6;

        //Act
        var namesFlux = fluxAndMonoGeneratorService.namesFluxApplyTransformSwitchIfEmpty(filterLength);

        //Assert
        StepVerifier.create(namesFlux)
                .expectNext("D", "E", "F", "A", "U", "L", "T")
                .verifyComplete();
    }

    @Test
    void exploreConcat() {
        //Arrange

        //Act
        var concatFlux = fluxAndMonoGeneratorService.exploreConcat();

        //Assert
        StepVerifier.create(concatFlux)
                .expectNext("A", "B", "C", "D", "E", "F")
                .verifyComplete();
    }

    @Test
    void exploreMerge() {
        //Arrange

        //Act
        var concatFlux = fluxAndMonoGeneratorService.exploreMerge();

        //Assert
        StepVerifier.create(concatFlux)
                .expectNext("A", "D", "B", "E", "C", "F")
                .verifyComplete();
    }

    @Test
    void exploreMergeSequential() {
        //Arrange

        //Act
        var concatFlux = fluxAndMonoGeneratorService.exploreMergeSequential();

        //Assert
        StepVerifier.create(concatFlux)
                .expectNext("A", "B", "C", "D", "E", "F")
                .verifyComplete();
    }

    @Test
    void exploreMergeZip() {
        //Arrange

        //Act
        var concatFlux = fluxAndMonoGeneratorService.exploreZip();

        //Assert
        StepVerifier.create(concatFlux)
                .expectNext("AD", "BE", "CF")
                .verifyComplete();
    }

    @Test
    void exploreMergeZipTuple() {
        //Arrange

        //Act
        var concatFlux = fluxAndMonoGeneratorService.exploreZipTuple();

        //Assert
        StepVerifier.create(concatFlux)
                .expectNext("AD14", "BE25", "CF36")
                .verifyComplete();
    }
}